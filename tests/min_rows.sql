WITH
    counts AS (SELECT COUNT(*) AS cnt FROM {{ ref('stg_payments') }})
SELECT cnt FROM counts WHERE cnt = 0
