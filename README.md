# dbt workshop

This is a workshop to showcase how to use dbt and basic dbt features.

It's heavily based on the [jaffle_shop](https://github.com/dbt-labs/jaffle_shop) example project of dbt.
All source code is copied from the jaffle_shop repository and modified in some places.

## Install

### dbt

```sh
virtualenv .venv
source .venv/bin/activate
pip install -r requirements.txt
```

### DuckDB CLI

#### Linux

```sh
wget https://github.com/duckdb/duckdb/releases/download/v0.5.1/duckdb_cli-linux-amd64.zip \
  && unzip duckdb*.zip \
  && rm duckdb*.zip
```

#### MacOS

```sh
wget https://github.com/duckdb/duckdb/releases/download/v0.5.1/duckdb_cli-osx-universal.zip  \
  && unzip duckdb*.zip \
  && rm duckdb*.zip
```

### Init DB

```sh
cp init.duckdb dbt.duckdb
```

## Workshop

### Prerequisite knowledge

- executing commands
- basic SQL
- basic Git

### Way of working

Every exercise:

1. I show an example.
2. You do an exercise.
3. I push a solution to Git.
4. You can `git pull` to be able to continue.

### Verify setup

```sh
dbt debug
```

Check database:

```sh
./duckdb dbt.duckdb
```

then

```sh
.tables
```

```sh
SELECT * FROM raw_customers;
```

### Exercise 1: Create a model and run it

See example model at `models/staging/stg_customers` and run it:

```sh
dbt run -s stg_customers
```

Check database:

```sh
SELECT * FROM stg_customers LIMIT 10;
```

Run all the models:

```sh
dbt run
```

**Exercise 1:** _Create a model named `stg_payments` that_
1. _renames the `id` column to `payment_id` in `raw_payments` table and_
2. _converts the payment `amount` to cents from dollars (`amount` is currently stored in cents)._

### Exercise 2: Refer to other models

Check out `models/customers.sql` for an example of how to refer to other models.

**Exercise 2:** _Create a model named `statuses` that shows how many orders we have per order status._

Expected output:
```
D SELECT * FROM statuses;
┌────────────────┬─────┐
│     status     │ cnt │
├────────────────┼─────┤
│ returned       │ 4   │
│ completed      │ 67  │
│ return_pending │ 2   │
│ shipped        │ 13  │
│ placed         │ 13  │
└────────────────┴─────┘
```

### Exercise 3: Use `table` materialization

Check how models are views so far, in DuckDB:

```sh
.schema
```

Example: materialize `stg_payments` as a table by adding this to the beginning of `stg_payments.sql`:

```
{{
    config(
        materialized='table'
    )
}}
```

**Exercise 3:**
_Materialize `statuses` as a table._
_What materializations would make sense for `stg_payments` and `statuses`?_

### Exercise 4: document columns

See `models/schema.yml` for how `customer_id` column is documented.

Generate documentation:
```sh
dbt docs generate
```

Display it in browser:
```sh
dbt docs serve
```

**Exercise 4:** _Document that `first_name` and `last_name` are PII columns._

### Exercise 5: add data tests

See `unique` and `not_null` tests in `models/staging/schema.yml`.

Run tests:

```sh
dbt test -s stg_customers
```

Run models and tests:

```sh
dbt build
```

**Exercise 5a:** _Test that there are no nulls in `payment_method` column_.

**Exercise 5b:** _Add a `unique` test to `payment_method` column that is going to fail. See what happens with `dbt build`._

### Exercise 6: add a custom test

Every SQL file in `tests` directory is a custom test.
The test fails if SQL query output has more than 0 rows.

Check out `tests/min_rows.sql` for an example.

```sh
dbt test -s min_rows
```

**Exercise 6:** _Add and run a test that verifies that there are no orders before `2000-01-01`._

### Exercise 7: Add a macro

See `macros/get_season.sql` as an example, used in `models/staging/stg_orders.sql`.

**Exercise 7:** _Add a macro named `cents_to_dollars` that converts cents to dollars. Use it._

### Bonus: tagging PII columns and printing finding them with a macro

See how to add a column tag in `models/staging/schema.yml`.

See `macros/print_pii.sql`. Execute it:

```sh
dbt run-operation print_pii
```

## Troubleshooting

### I cannot `git pull` because of my own changes

Execute `git checkout .` to throw away your own changes.

### Starting with clean DB

```sh
cp init.duckdb dbt.duckdb
```
