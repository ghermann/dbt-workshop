with source as (

    select * from raw_orders

),

renamed as (

    select
        id as order_id,
        user_id as customer_id,
        order_date,
        status,
        {{ get_season('order_date') }} AS season

    from source

)

select * from renamed
