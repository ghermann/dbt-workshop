{{
    config(
        materialized='table'
    )
}}

WITH
    orders AS (SELECT * FROM {{ ref('stg_orders') }})

SELECT
    status,
    COUNT(status) AS cnt
FROM orders
GROUP BY
    status
