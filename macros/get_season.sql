{% macro get_season(column_name) %}
    (CASE
    WHEN {{ column_name }} >= '2018-03-01' AND {{ column_name }} < '2018-06-01' THEN 'spring'
    WHEN {{ column_name }} >= '2018-06-01' AND {{ column_name }} < '2018-09-01' THEN 'summer'
    WHEN {{ column_name }} >= '2018-09-01' AND {{ column_name }} < '2018-12-01' THEN 'autumn'
    ELSE 'winter'
    END)
{% endmacro %}
