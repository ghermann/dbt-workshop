{% macro print_pii() %}
{% if execute %}
  {% for node in graph.nodes.values()
      | selectattr("resource_type", "equalto", "model") %}
    {% for col in node.columns.values() %}
      {% for tag in col.tags %}
        {% if tag == "pii" %}
          {% do log("PII column: " ~ node.unique_id ~ "." ~ col.name, info=true) %}
        {% endif %}
      {% endfor %}
    {% endfor %}
  {% endfor %}
{% endif %}
{% endmacro %}
